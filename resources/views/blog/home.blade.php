@extends('layout');

@section('secondHeader')
    @if ($featuredPosts->count() == 2)
        <div class="row mb-2">
            @each('blog/include/featuredPost', $featuredPosts, 'post')
        </div>
    @endif
@endsection

@section('main')
    <div class="row">
        @include('blog/include/posts')
        @include('blog/include/sidebar')
    </div>
@endsection

@section('js')
    <script>
        Holder.addTheme('thumb', {
            bg: '#55595c',
            fg: '#eceeef',
            text: 'Thumbnail'
        });
    </script>
@endsection
