@extends('layout');

@section('main')
    <div class="row">
        @include('blog/include/posts')
        @include('blog/include/sidebar')
    </div>
@endsection
