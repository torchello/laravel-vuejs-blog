<aside class="col-md-4 blog-sidebar">
    <div class="p-3">
        @if ($periods)
            <h4 class="font-italic">Archives</h4>
            <ol class="list-unstyled mb-0">
                @foreach ($periods as $period)
                    <li>
                        <a href="{{ route('showPostsByPeriod', ['period' => $period->format('Ym')]) }}">
                            {{ $period->format('F Y') }}
                        </a>
                    </li>
                @endforeach
            </ol>
        @endif
    </div>
</aside>
