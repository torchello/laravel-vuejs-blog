<div class="col-md-8 blog-main">
    @forelse ($posts as $post)
        @include ('blog/include/postPreview')
    @empty
        <h2>No posts found</h2>
    @endforelse

    @if ($posts)
        <nav class="blog-pagination">
            {{ $posts->links() }}
        </nav>
    @endif
</div>
