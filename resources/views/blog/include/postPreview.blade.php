<div class="blog-post">
    @include('blog/include/postHeader')

    <div>
        {{ strip_tags(str_limit($post->text, 500)) }}
        <div>
            <a href="{{ route('showPost', ['id' => $post->id]) }}" class="btn btn-primary read-more">Read more</a>
        </div>
    </div>
</div>
