<div class="col-md-6">
    <div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
            <strong class="d-inline-block mb-2 text-success">Featured</strong>
            <h3 class="mb-0">
                <a class="text-dark" href="#">{{ $post->title }}</a>
            </h3>
            <div class="mb-1 text-muted">{{ $post->created_at->diffForHumans() }}</div>
            <p class="card-text mb-auto">
                {{ str_limit($post->text, 100) }}
            </p>
            <a href="{{ route('showPost', ['id' => $post->id]) }}">Continue reading</a>
        </div>
        <img class="card-img-right flex-auto d-none d-lg-block" data-src="holder.js/200x250?theme=thumb" alt="Card image cap">
    </div>
</div>
