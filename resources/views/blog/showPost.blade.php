@extends('layout');

@section('main')
    <div class="row">
        <div class="col-md-8 blog-main">
            @include('blog/include/post')
        </div>
        @include('blog/include/sidebar')
    </div>
@endsection
