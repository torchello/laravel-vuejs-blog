@extends('layout')

@section('main')
    <div class="container admin">
        <h2>
            @yield('title')
        </h2>
        @include('admin/include/form')
    </div>
@endsection

@section('js')
    <script>
        ClassicEditor.create(document.querySelector('#post-text'));
    </script>
@endsection
