@extends('layout')

@section('main')
    <div class="row">
        <a href="{{ route('adminCreatePost') }}" class="btn btn-primary" id="create-post">Create post</a>

        @if ($posts)
            @include ('admin/include/postsTable')
        @else
            <h2>No posts found</h2>
        @endif

        @if ($posts)
            <nav class="blog-pagination">
                {{ $posts->links() }}
            </nav>
        @endif
    </div>
@endsection
