@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="error-list">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form method="POST">
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" class="form-control" value="{{ !empty($post) ? $post->title : '' }}">
    </div>
    <div class="form-group">
        <label for="post-text">Text</label>
        <textarea class="form-control" name="text" id="post-text">
            {{ !empty($post) ? $post->text : '' }}
        </textarea>
    </div>
    <div class="form-check">
        <input
            class="form-check-input"
            name="featured"
            type="checkbox"
            value="1"
            {{ !empty($post) && $post->featured ? 'checked' : '' }}
        >
        <label class="form-check-label" for="featured">
            Featured post
        </label>
    </div><br />
    {{ csrf_field() }}
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="{{ route('admin') }}" class="btn">Back</a>
</form>
