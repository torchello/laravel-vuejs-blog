<table class="table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>TItle</th>
            <th>Author</th>
            <th>Created</th>
            <th>Updated</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($posts as $post)
        <tr>
            <td>{{ $post->id }}</td>
            <td>{{ $post->title }}</td>
            <td>{{ $post->user->name }}</td>
            <td>{{ $post->created_at }}</td>
            <td>{{ $post->updated_at }}</td>
            <td>
                <a class="btn-sm btn-primary" href="{{ route('adminEditPost', ['id' => $post->id]) }}">Edit</a>
                <a
                    class="btn-sm btn-danger"
                    href="{{ route('adminDeletePost', ['id' => $post->id]) }}"
                    onClick="return confirm('Are you sure you want to delete this post?')"
                >
                    Delete
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
