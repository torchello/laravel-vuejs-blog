<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrEditPost;
use App\Post;
use App\Repository\PostRepository;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class AdminController extends BaseController
{
    public function listPosts(PostRepository $postRepository)
    {
        return view('admin/listPosts', [
            'posts' => $postRepository->getForAdmin()
        ]);
    }

    public function createPost()
    {
        return view('admin/createPost');
    }

    public function processCreatePostRequest(CreateOrEditPost $request)
    {
        $post = new Post();
        $post->setFromRequest($request);
        $post->user_id = Auth::user()->getAuthIdentifier();

        $post->save();

        return redirect('admin');
    }

    public function editPost($id)
    {
        return view('admin/editPost', [
            'post' => Post::findOrFail($id),
        ]);
    }

    public function processEditPostRequest($id, CreateOrEditPost $request)
    {
        /** @var Post $post */
        $post = Post::findOrFail($id);
        $post->setFromRequest($request);
        $post->save();

        return redirect('admin');
    }

    public function deletePost($id)
    {
        /** @var Post $post */
        $post = Post::findOrFail($id);
        $post->delete();

        return redirect('admin');
    }
}
