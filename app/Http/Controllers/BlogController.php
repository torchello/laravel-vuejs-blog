<?php

namespace App\Http\Controllers;

use App\Post;
use App\Repository\PostRepository;
use Illuminate\Routing\Controller as BaseController;

class BlogController extends BaseController
{
    public function home(PostRepository $postRepository)
    {
        return view('blog/home', [
            'posts' => $postRepository->getForHomepage(),
            'featuredPosts' => $postRepository->getFeaturedForHomepage(),
            'periods' => $postRepository->getAvailablePeriods(),
        ]);
    }

    public function showPost(int $id, PostRepository $postRepository)
    {
        return view('blog/showPost', [
            'post' => Post::findOrFail($id),
            'periods' => $postRepository->getAvailablePeriods(),
        ]);
    }

    public function showPostsByPeriod(string $period, PostRepository $postRepository)
    {
        if (!$periodDateTime = \DateTime::createFromFormat('Ym', $period)) {
            abort(404);
        }

        return view('blog/showPosts', [
            'posts' => $postRepository->getForPeriod($periodDateTime),
            'periods' => $postRepository->getAvailablePeriods(),
        ]);
    }
}
