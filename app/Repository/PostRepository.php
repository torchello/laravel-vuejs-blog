<?php

namespace App\Repository;

use App\Post;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PostRepository
{
    const BLOG_POSTS_PER_PAGE = 5;
    const ADMIN_POSTS_PER_PAGE = 10;

    public function getForAdmin()
    {
        return Post::orderBy('created_at', 'desc')->paginate(self::ADMIN_POSTS_PER_PAGE);
    }

    public function getForHomepage()
    {
        return Post::orderBy('created_at', 'desc')->paginate(self::BLOG_POSTS_PER_PAGE);
    }

    public function getFeaturedForHomepage()
    {
        return Post::where('featured', true)->inRandomOrder()->limit(2)->get();
    }

    public function getAvailablePeriods(): Collection
    {
        return DB::table('posts')
            ->selectRaw('DISTINCT DATE_FORMAT(created_at, \'%Y-%m\') AS period')
            ->orderBy('period', 'desc')
            ->get()
            ->map(function($value) {
                return new \DateTime($value->period);
            });
    }

    public function getForPeriod(\DateTime $period)
    {
        return Post::query()
            ->whereRaw('YEAR(created_at) = :year')
            ->whereRaw('MONTH(created_at) = :month')
            ->orderBy('created_at', 'desc')
            ->setBindings([
                'year' => $period->format('Y'),
                'month' => $period->format('m'),
            ])
            ->paginate(self::BLOG_POSTS_PER_PAGE);
    }
}
