<?php

namespace App;

use App\Http\Requests\CreateOrEditPost;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $title;
 * @property string $text;
 * @property bool $featured;
 * @property User $user;
 * @property int $user_id;
 * @property \DateTime $created_at;
 * @property \DateTime $updated_at;
 */
class Post extends Model
{
    /** @var string */
    protected $table = 'posts';

    /** @var string[] */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setFromRequest(CreateOrEditPost $request)
    {
        $data = $request->validated();

        $this->title = $data['title'];
        $this->text = $data['text'];
        $this->featured = isset($data['featured']) && $data['featured'];
    }
}
