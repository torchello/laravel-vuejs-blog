<?php

namespace App\Providers;

use App\Repository\PostRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
    }

    public function register()
    {
        $this->app->bind(PostRepository::class, function () {
            return new PostRepository();
        });
    }
}
