# Installation
1. `git clone https://gitlab.com/torchello/laravel-vuejs-blog`
2. `cd laravel-vuejs-blog`
3. `composer install`
4. `yarn install`
5. `cp .env.example .env`
6. `php artisan key:generate`
7. Update `DB_*` values in `.env` and create appropriate database
8. `php artisan serve`
9. Open http://127.0.0.1:8000/
10. Enjoy! :)

## Admin panel credentials
Login: `demo@example.com`
Password: `secret`