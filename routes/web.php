<?php

Route::get('/', 'BlogController@home')->name('home');
Route::get('/post/{id}', 'BlogController@showPost')->name('showPost');
Route::get('/posts/{period}', 'BlogController@showPostsByPeriod')->name('showPostsByPeriod');

Route::get('/admin', 'AdminController@listPosts')->name('admin')->middleware('auth');

Route::get('/admin/create', 'AdminController@createPost')->name('adminCreatePost')->middleware('auth');
Route::post('/admin/create', 'AdminController@processCreatePostRequest')
    ->name('adminProcessCreatePost')
    ->middleware('auth');

Route::get('/admin/edit/{id}', 'AdminController@editPost')->name('adminEditPost')->middleware('auth');
Route::post('/admin/edit/{id}', 'AdminController@processEditPostRequest')
    ->name('adminProcessEditPost')
    ->middleware('auth');

Route::get('/admin/delete/{id}', 'AdminController@deletePost')->name('adminDeletePost')->middleware('auth');

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
