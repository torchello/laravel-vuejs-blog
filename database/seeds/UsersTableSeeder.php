<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    const NAME = 'demo';
    const EMAIL = 'demo@example.com';
    const PASSWORD = 'secret';

    public function run()
    {
        DB::table('users')->insert([
            'name' => self::NAME,
            'email' => self::EMAIL,
            'password' => bcrypt(self::PASSWORD),
            'remember_token' => str_random(10),
        ]);
    }
}
