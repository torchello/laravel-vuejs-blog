<?php

use App\Post;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(30),
        'text' => $faker->realText(2000),
        'featured' => $faker->boolean,
        'created_at' => $faker->dateTimeBetween('-1 year'),
        'user_id' => 1,
    ];
});
